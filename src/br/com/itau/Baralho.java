package br.com.itau;

public class Baralho {
    private int topo;
    private Carta monte[];

    public Baralho() {
        monte = new Carta[52];
        topo = 0;

        for(int naipe=1; naipe < 5; naipe++){
            for (int valor=1; valor < 14; valor++){
                monte[topo++] = new Carta(naipe, valor);
            }
        }
    }

    public void embaralhar(){
       for(int contador =0; contador < monte.length; contador++) {
           int valor = (int) Math.round(Math.random() * 51);
           Carta carta = monte[contador];
           monte[valor] = monte[contador];
           monte[contador] = carta;
       }
    }

    public boolean verificarCarta(){
        return topo > 0;
    }

    public Carta proximaCarta(){
        Carta carta = null;
        if(topo > 0){
            carta = monte[--topo];
        }
        return carta;
    }
}
