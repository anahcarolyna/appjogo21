package br.com.itau;


import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Historico extends Jogador {

    private int pontuacaoMaximaAtingida = 0;
    List<Historico> historico;

    public Historico(String nome, int pontos) {
        super(nome, pontos);
        historico = new ArrayList<>();
        this.pontuacaoMaximaAtingida = pontos;
    }

    public Historico() {

    }

    public void setPontuacaoMaximaAtingida(int pontuacaoMaximaAtingida) {
        this.pontuacaoMaximaAtingida = pontuacaoMaximaAtingida;
    }

    public void AdicionarHistorico(Historico historico){
        this.historico.add(historico);
    }
    public int getPontuacaoMaximaAtingida() {
        return pontuacaoMaximaAtingida;
    }

    public String imprimirRecorde(){
        return this.getNome() + getPontuacaoMaximaAtingida();
    }

    public void incluirHistorico(){
        for(Historico historico : historico){
            if(historico.getNome().equalsIgnoreCase(this.getNome()) &&
                    historico.getPontuacaoMaximaAtingida() > this.getPontos()){
                JOptionPane.showMessageDialog(null, "Recorde atual: " + historico.getPontuacaoMaximaAtingida());
            }
            else{
                historico.AdicionarHistorico(historico);
                JOptionPane.showMessageDialog(null, "Novo recorde de pontuação: " + this.getPontos());
            }
        }
    }
}
