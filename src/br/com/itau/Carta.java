package br.com.itau;

public class Carta {
    public static final int PAUS = 1;
    public static final int OUROS = 2;
    public static final int ESPADAS = 3;
    public static final int COPAS = 4;

    private int naipe;
    private int valor;

    public Carta(int naipe, int valor) {
        if (naipe >= this.PAUS && naipe <= COPAS) {
            this.naipe = naipe;
        } else {
            this.naipe = 1;
        }

        if (valor >= 1 && naipe < 13) {
            this.valor = valor;
        } else {
            this.valor = 1;
        }
    }

    public int getNaipe() {
        return naipe;
    }

    public void setNaipe(int naipe) {
        this.naipe = naipe;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String imprimirCartas(){
        String retorno = null;
        switch(valor){
            case 1: retorno = "as"; break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10: retorno = valor +""; break;
            case 11: retorno = "valete"; break;
            case 12: retorno = "dama"; break;
            case 13: retorno = "rei"; break;
        }

        switch(naipe){
            case PAUS: return retorno + " de paus";
            case OUROS: return retorno + " de ouros";
            case ESPADAS: return retorno + " de espadas";
            case COPAS: return retorno + " de copas";
        }

        return retorno;
    }
}
